Cambios solicitados
1. Dejar el producto combo con valor 0 y cada producto con su propio precio
2. En las lineas de la orden aumentar un campo que indique que productos vienen de un combo
3. Generar un reporte excel o pdf de los productos

![1_2.PNG](https://gitlab.com/jemonarc/odoo_pos/-/raw/master/1_2.PNG)
![3.PNG](https://gitlab.com/jemonarc/odoo_pos/-/raw/master/3.PNG)




