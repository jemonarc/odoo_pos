# -*- coding: utf-8 -*-
import psycopg2
from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo import SUPERUSER_ID
from functools import partial


class PosOrderCb(models.Model):
    _inherit = 'pos.order'

    @api.model
    def _process_order(self, order, draft, existing_order):
        """Create or update an pos.order from a given dictionary.

        :param pos_order: dictionary representing the order.
        :type pos_order: dict.
        :param draft: Indicate that the pos_order is not validated yet.
        :type draft: bool.
        :param existing_order: order to be updated or False.
        :type existing_order: pos.order.
        :returns number pos_order id
        """

        order = order['data']
        pos_session = self.env['pos.session'].browse(order['pos_session_id'])
        if pos_session.state == 'closing_control' or pos_session.state == 'closed':
            order['pos_session_id'] = self._get_valid_session(order).id
        pos_order = False
        if not existing_order:
            pos_order = self.create(self._order_fields(order))
        else:
            pos_order = existing_order
            pos_order.lines.unlink()
            order['user_id'] = pos_order.user_id.id
            pos_order.write(self._order_fields(order))
        vals = {}

        # Update pos orders lines of product with is pack = True
        for pols in pos_order.lines:
            if pols.product_id.is_pack:
                pols.write({'price_unit': 0, 'price_subtotal': 0, 'price_subtotal_incl': 0})

        if order['lines']:
            print(order['lines'])
            for l in order['lines']:
                if 'is_pack' in l[2]:
                    if l[2]['is_pack']:
                        for prod in l[2]['combo_prod_ids']:
                            product_id = self.env['product.product'].browse(prod)
                            print(product_id.name)
                            self.env['pos.order.line'].create({
                                'name': self.env['ir.sequence'].next_by_code('pos.order.line'),
                                'discount': 0,
                                'product_id': product_id.id,
                                'price_subtotal': product_id.list_price * l[2]['qty'],
                                'price_unit': product_id.list_price,
                                'order_id': pos_order.id,
                                'qty': l[2]['qty'],
                                'price_subtotal_incl': product_id.list_price * l[2]['qty'],
                                'product_pack_id': l[2]['product_id']
                            })
        # import pdb; pdb.set_trace()
        self._process_payment_lines(order, pos_order, pos_session, draft)

        if not draft:
            try:
                pos_order.action_pos_order_paid()
            except psycopg2.DatabaseError:
                # do not hide transactional errors, the order(s) won't be saved!
                raise
            except Exception as e:
                _logger.error('Could not fully process the POS Order: %s', tools.ustr(e))

        if pos_order.to_invoice and pos_order.state == 'paid':
            pos_order.action_pos_order_invoice()
            pos_order.account_move.sudo().with_context(force_company=self.env.user.company_id.id).post()

        return pos_order.id

    def action_print_pdf(self):
        return self.env.ref('bi_pos_combo2.pos_order_report').report_action(self)

class PosOrderLineCb(models.Model):
    _inherit = "pos.order.line"

    product_pack_id = fields.Many2one('product.product', string='Pack', required=False)

