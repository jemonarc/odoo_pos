# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class PosOrderReport(models.AbstractModel):
    _name = 'report.bi_pos_combo2.print_pos_order'

    @api.model
    def _get_report_values(self, docids, data):
        return {'docs': self.env['pos.order'].browse(docids)}